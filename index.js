//S30 Activity

//Same database

//inserting data
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);


// (1)

//using count operator to count the total num of fruits on sale

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "onSale fruits"}

	]);

//(2)

//using count operator and $gte
db.fruits.aggregate([
	{$match: {stock:{$gte: 20}}},
	{$count: "enoughStock"}

	]);

//(3)

//using the average operato to get the ave price

db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group: {_id: 1, _id: "$supplier_id", avgPrice: {$avg: "$price"}}
		}

	]);

//(4)

//using max operator
db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group: {_id: 1, _id: "$supplier_id", maxPrice: {$max: "$price"}}
		}

	]);
//(5)
//using min operator

db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group: {_id: 1, _id: "$supplier_id", minPrice: {$min: "$price"}}
		}

	]);





